package co.com.zenware.shoppinglist.service;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by John on 4/12/2015.
 */
@Root(name = "ShoppingBook")
@Namespace(reference="http://domain/shoppingbook")
public class ShoppingBook {
    @ElementList(name ="shopping_list" ,inline = true,empty=false)
    @Getter@Setter
    private List<ShoppingList> shopping_list;


}
