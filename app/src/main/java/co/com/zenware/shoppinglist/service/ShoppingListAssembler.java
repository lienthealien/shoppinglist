package co.com.zenware.shoppinglist.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by John on 4/13/2015.
 */
public class ShoppingListAssembler {
    @Getter
    @Setter
  private ShoppingList shopping_list;
    @Getter
    @Setter
  private   List<String> mdl_headers;
    @Getter
    @Setter
  private   HashMap<String,List<Item>> mdl_data_items;
    @Getter
    @Setter
  private double list_price=0.0;

    public ShoppingListAssembler(ShoppingList shopping_list) {
        this.shopping_list = shopping_list;
        mdl_headers =new ArrayList<>();
        mdl_data_items =new HashMap<>();
    }

    public void loadHeaders(){
        mdl_headers.clear();mdl_data_items.clear();
        for(Item item:shopping_list.getItems_list()){
            mdl_headers.add(item.getItem_name());
            mdl_data_items.put(item.getItem_name(),new ArrayList<Item>(Arrays.asList(item)));
        }

    }

    public Double getListPrice(){
        Double total=0.0;
        for(Item item: shopping_list.getItems_list()){
            total+=item.getPrice();
        }
        shopping_list.setTotal_price(total);
        return total;
    }

    public String addItem(String name){
        Item newItem=new Item();
        newItem.setItem_name(name);
        String item_code=newItem.hashCode()+"";
        newItem.setId(item_code);
        shopping_list.getItems_list().add(newItem);
        updateListModel(newItem);
        return item_code;
    }

    public String addItem(Item new_item){
        boolean item_idexist_count=mdl_data_items.containsKey(new_item.getItem_name());
        if(item_idexist_count){ new_item.setItem_name(new_item.getItem_name()+""+new_item.hashCode());}
        new_item.setId(new_item.getItem_name());
        shopping_list.getItems_list().add(new_item);
        updateListModel(new_item);
        setList_price(getListPrice());
        return new_item.getId();

    }

    public void updateListModel(Item newItem){
        if(!mdl_headers.contains(newItem.getId())){mdl_headers.add(newItem.getId());}
        mdl_data_items.put(newItem.getId(), new ArrayList<Item>(Arrays.asList(newItem)));
    }

    public void removeItem(String id){
        int idx=-1;
        for(Item item:shopping_list.getItems_list()){
            idx++;
            if(item.getItem_name().equals(id)){break; }
        }
        if(idx>=0){shopping_list.getItems_list().remove(idx);loadHeaders();}
        setList_price(getListPrice());
    }



    public void updateItem(String id,Item updated_item){
        int idx=-1;

         updated_item.setId(getMdl_data_items().get(id).get(0).getId());
         updated_item.setItem_name(getMdl_data_items().get(id).get(0).getItem_name());
        for(Item item:shopping_list.getItems_list()){
            idx++;
            if(item.getItem_name().equals(id)){break; }
        }
        if(idx>=0){
        loadHeaders();
        shopping_list.getItems_list().get(idx).setCheck(updated_item.getCheck());
        updateListModel(updated_item);
        setList_price(getListPrice());
        }

     }


}
