package co.com.zenware.shoppinglist;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.File;
import java.util.ArrayList;

import co.com.zenware.shoppinglist.persistance.ShoppingBookPersiter;
import co.com.zenware.shoppinglist.service.Item;
import co.com.zenware.shoppinglist.service.ShoppingBook;
import co.com.zenware.shoppinglist.service.ShoppingList;
import co.com.zenware.shoppinglist.ui.MyListAdapter;


public class MainActivity extends ActionBarActivity implements View.OnClickListener  {
    final Context context = this;
    MyListAdapter mylist_adapter;
    ShoppingBook shopping_book;
    ExpandableListView explv_shopping_list;
    ShoppingList shopping_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        explv_shopping_list=(ExpandableListView) findViewById(R.id.exp_shoppinglist);
        createSampleShoppingBook();
        mylist_adapter=new MyListAdapter(context,shopping_book,this);
        explv_shopping_list.setAdapter(mylist_adapter);
        findViewById(R.id.btn_additmem).setOnClickListener(this);
        mylist_adapter.updateModel();

    }

    public void createSampleShoppingBook(){
        if(new File(context.getFilesDir(),ShoppingBookPersiter.FILE_NAME).exists()){
           try{shopping_book=ShoppingBookPersiter.loadList(context);}

           catch(Exception e){
               shopping_book=new ShoppingBook();
               shopping_book.setShopping_list(new ArrayList<ShoppingList>());
               shopping_list=new ShoppingList();
               shopping_list.setList_name("Groceries");
               shopping_list.setItems_list(new ArrayList<Item>());
               shopping_book.getShopping_list().add(shopping_list);
           }

        }else{
            shopping_book=new ShoppingBook();
            shopping_book.setShopping_list(new ArrayList<ShoppingList>());
            shopping_list=new ShoppingList();
            shopping_list.setList_name("Groceries");
            shopping_list.setItems_list(new ArrayList<Item>());
            shopping_book.getShopping_list().add(shopping_list);


        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btn_additmem:
                CharSequence newitem_name=((TextView)findViewById(R.id.field_newitemname)).getText();
                CharSequence newitem_price=((TextView) findViewById(R.id.field_price)).getText();
                if (newitem_name.length()>3&&!newitem_name.toString().trim().isEmpty()) {
                    Item new_item=new Item();
                    new_item.setId(newitem_name.toString());
                    new_item.setItem_name(newitem_name.toString());
                    new_item.setCheck(new Boolean(false));
                    new_item.setDescription("--");
                    try{
                        new_item.setPrice(Double.parseDouble(newitem_price.toString()));
                        mylist_adapter.addItem(new_item);
                    }catch (Exception e){
                        Log.i("addItemError",e.getMessage());
                    }
                }
                break;

        }
    }


}
