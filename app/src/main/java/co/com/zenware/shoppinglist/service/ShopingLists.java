package co.com.zenware.shoppinglist.service;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by John on 4/12/2015.
 */
@Root(name = "shoppinglists")
public class ShopingLists {
    @ElementList(inline = true,empty=false)
    public List<ShopList> shop_list;
}
