package co.com.zenware.shoppinglist.ui;

/**
 * Created by John on 4/14/2015.
 */
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import co.com.zenware.shoppinglist.R;
import co.com.zenware.shoppinglist.persistance.ShoppingBookPersiter;
import co.com.zenware.shoppinglist.service.Item;
import co.com.zenware.shoppinglist.service.ShoppingBook;
import co.com.zenware.shoppinglist.service.ShoppingListAssembler;
import lombok.Getter;
import lombok.Setter;


/**
 * Created by John on 4/9/2015.
 */
public class MyListAdapter extends BaseExpandableListAdapter {

    @Getter@Setter
    private List<String> _listDataHeader; // header titles
    @Getter@Setter
    private HashMap<String, List<Item>> _listDataChild;
    @Getter@Setter
    private ShoppingBook shoppingBook;
    @Getter@Setter
    private ShoppingListAssembler shoppingListAssembler;
    @Getter@Setter
    private Context _context;
    @Getter@Setter
    private Activity _activity;

    public MyListAdapter(Context context,ShoppingBook shoppingBook,Activity activity) {
       this.shoppingBook=shoppingBook;
       this._context = context;
       this._activity=activity;
       shoppingListAssembler=new ShoppingListAssembler( this.shoppingBook.getShopping_list().get(0));
       this._context = context;
       updateModel();
       sampleData();
    }

    public void sampleData(){
        if(!new File(_context.getFilesDir(),ShoppingBookPersiter.FILE_NAME).exists()){
       Item item1=new Item();
       item1.setItem_name("Kama");
       item1.setId("Kama");
       item1.setPrice(0.75);
       item1.setDescription("For breakfast");
       item1.setCheck(new Boolean(true));
       Item item2=new Item();
       item2.setItem_name("Bread");
       item2.setId("Bread");
       item2.setPrice(1.25);
       item2.setCheck(new Boolean(false));
       item2.setDescription("For chesse and jam");
       Item item3=new Item();
       item3.setItem_name("Apples");
       item3.setId("Apples");
       item3.setPrice(0.25);
       item3.setCheck(new Boolean(false));
       item3.setDescription("one a day ");
       addItem(item1);
       addItem(item2);
       addItem(item3);
       ShoppingBookPersiter.saveList(_context, shoppingBook);
        }
    }

    public void updateModel(){
        shoppingListAssembler.loadHeaders();
        _listDataHeader= shoppingListAssembler.getMdl_headers();
        _listDataChild=shoppingListAssembler.getMdl_data_items();
        notifyDataSetChanged();
        TextView list_price= (TextView) _activity.findViewById(R.id.txt_price);
        list_price.setText("$"+shoppingListAssembler.getList_price());

         }
    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView( int groupPosition,  int childPosition,boolean isLastChild, View convertView, ViewGroup parent) {
        Item childText = (Item) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item, null);
        }
        if(childText!=null){
            TextView item_name=(TextView)convertView.findViewById(R.id.item_name);
            item_name.setText(childText.getItem_name());
            TextView item_price=(TextView)convertView.findViewById(R.id.item_price);
            item_price.setText(childText.getPrice()+"");
        }
        return convertView;
    }
    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    public long getGroupIdByHeader(String header_tag) {
        long position=-1L;
        for(int idx=0;idx<_listDataHeader.size();idx++){
            if(header_tag.equals(((String)getGroup(idx)))){
                position=idx;
                break;
            }
        }
        return position;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        TextView list_price= (TextView) _activity.findViewById(R.id.txt_price);
        list_price.setText("$"+shoppingListAssembler.getList_price());
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }
        if(headerTitle!=null){
        Boolean item_checked= ((List<Item>)get_listDataChild().get(headerTitle)).get(0).getCheck();
        TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);
        Button delete_item=(Button)convertView.findViewById(R.id.btn_delete_item);
        CheckBox cbx_item_checked=(CheckBox) convertView.findViewById(R.id.item_check);
        if(cbx_item_checked!=null){
            cbx_item_checked .setFocusable(false);
            CheckListener check_listener = new CheckListener(headerTitle,groupPosition);
            cbx_item_checked.setOnCheckedChangeListener(check_listener);
            delete_item.setOnClickListener(check_listener);
            try{
                cbx_item_checked.setChecked(item_checked);
               // Toast.makeText(_context,"groupPosition "+groupPosition+" headerTitle -> "+headerTitle+" "+item_checked,Toast.LENGTH_LONG).show();
            }catch(Exception e){
             Toast.makeText(_context,"Null "+groupPosition+" => "+item_checked,Toast.LENGTH_LONG).show();
            }
        }
       }
      return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public class CheckListener implements CompoundButton.OnCheckedChangeListener, Button.OnClickListener {
        private String item_header;
        long pos;
        public CheckListener(String item_header,long pos) {
            this.pos = pos;
            this.item_header=item_header;
        }
        public CheckListener() { }
        @Override
        public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
            Log.i("checkListenerChanged", String.valueOf(pos) + ":" + String.valueOf(isChecked));
           // Boolean orignal_check=get_listDataChild().get(item_header).get(0).getCheck();
            //get_listDataChild().get(item_header).get(0).setCheck(isChecked);
            Item updated_item=new Item();updated_item.setCheck(new Boolean(isChecked));
            shoppingListAssembler.updateItem(item_header,updated_item);
            TextView list_price= (TextView) _activity.findViewById(R.id.txt_price);
            list_price.setText("$"+shoppingListAssembler.getList_price());
            ShoppingBookPersiter.saveList(_context,shoppingBook);
        }
        @Override
        public void onClick(View v) {
            Toast.makeText(_context,"DELETING "+item_header,Toast.LENGTH_SHORT).show();
            removeItem(item_header);
            TextView list_price= (TextView) _activity.findViewById(R.id.txt_price);
            list_price.setText("$"+shoppingListAssembler.getList_price());
        }
    }
    public void addItem(Item new_item){

        shoppingListAssembler.addItem(new_item);
        TextView list_price= (TextView) _activity.findViewById(R.id.txt_price);
        list_price.setText("$"+shoppingListAssembler.getList_price());

        updateModel();
        ShoppingBookPersiter.saveList(_context,shoppingBook);
    }
    public void removeItem(String item_header){
        get_listDataHeader().remove(item_header);
        get_listDataChild().remove(item_header);

        shoppingListAssembler.removeItem(item_header);
        updateModel();
        ShoppingBookPersiter.saveList(_context,shoppingBook);
    }
    public void updateItem(){}



}