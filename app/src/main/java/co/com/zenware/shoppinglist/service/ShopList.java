package co.com.zenware.shoppinglist.service;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by John on 4/12/2015.
 */
@Root(name = "shoplist")
public class ShopList {
    @Element(name = "name",type =String.class )
    private String name;
    @Element(name = "total_price",type =Double.class )
    private  Double total_price;
    @ElementList(inline = true,empty=false)
    private List<ShopItem> shop_item;



}
