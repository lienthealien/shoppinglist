package co.com.zenware.shoppinglist.service;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by John on 4/12/2015.
 */
@Root(name = "ShoppingList")
@Namespace(reference="http://domain/shoppingbook/shoppinglist")
public class ShoppingList {
    @Element(name = "list_name",type =String.class )
    @Getter@Setter
    private String list_name;
    @Element(name = "total_price",type =Double.class )
    @Getter@Setter
    private  Double total_price;
    @ElementList(name ="items_list",inline = true,empty=false)
    @Getter@Setter
    private List<Item> items_list;




}
