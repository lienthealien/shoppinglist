package co.com.zenware.shoppinglist.service;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by John on 4/12/2015.
 */
@Root(name = "shopitem")
public class ShopItem {
    @Element(name = "name",type =String.class)
    private String name;
    @Element(name = "price",type =Double.class )
    private Double price;
    @Element(name = "name",type =String.class)
    private String description;
}
