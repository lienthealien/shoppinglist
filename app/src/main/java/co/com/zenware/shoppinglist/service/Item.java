package co.com.zenware.shoppinglist.service;

import org.simpleframework.xml.Default;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

/**
 * Created by John on 4/12/2015.
 */
@Root(name = "item")
@Namespace(reference="http://domain/shoppingbook/shoplist/shopitem")
public class Item {
    @Element(name = "id", type = String.class,required = true)
    @Getter@Setter
    private String id;
    @Element(name = "item_name", type = String.class,required = true)
    @Namespace(reference="http://domain/shoppingbook/shoplist/shopitem", prefix="item")
    @Getter@Setter
    private String item_name;
    @Element(name = "price", type = Double.class)
    @Getter@Setter
    private Double price;
    @Element(name = "description", type = String.class)
    @Getter@Setter
    private String description;

    @Element(name = "check", type = Boolean.class,data =false )
    @Setter
    private Boolean check;


    public Boolean getCheck() {
        return check==null?new Boolean(false):check;
    }
}

