package co.com.zenware.shoppinglist.persistance;

import android.content.Context;
import android.util.Log;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.File;
import java.io.IOException;

import co.com.zenware.shoppinglist.service.ShoppingBook;

/**
 * Created by John on 4/20/2015.
 */
public class ShoppingBookPersiter {
    public static String FILE_NAME="ShoppingBook.xml";
    public static void saveList(Context _context,ShoppingBook shoppingBook){
        Serializer serializer = new Persister();
        File source;
        try {
            source = new File(_context.getFilesDir(),FILE_NAME);
            Log.i("Shopping APP PATH"," source.createNewFile() "+source.createNewFile()+" path "+source.getPath());
            serializer.write(shoppingBook, source);

        }catch(Exception e){
            e.printStackTrace();
       }
    }

    public static ShoppingBook loadList(Context _context){
        ShoppingBook shoppingBook;
        Serializer serializer = new Persister();

        File source;
        try {
            source = new File(_context.getFilesDir(),FILE_NAME);
            Log.i("Shopping APP PATH"," source.createNewFile() "+source.createNewFile()+" path "+source.getPath());


            shoppingBook = serializer.read(ShoppingBook.class, source);
            return shoppingBook;
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("Shopping APP PATH"," =>"+e.getMessage());
            return null;
        }
    }
}
