import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import co.com.zenware.shoppinglist.service.Item;
import co.com.zenware.shoppinglist.service.ShoppingBook;
import co.com.zenware.shoppinglist.service.ShoppingList;
import co.com.zenware.shoppinglist.service.ShoppingListAssembler;

/**
 * Created by John on 4/13/2015.
 */
public class ShoppingBookTest {
    public ShoppingBook shopping_book;
    public ShoppingList shopping_list;
    public Item item;
    @Test
    public void testShoppingBookCreation(){
        shopping_book=new ShoppingBook();
        shopping_list=new ShoppingList();
        shopping_list.setList_name("Groceries");
        shopping_book.setShopping_list(Arrays.asList(new ShoppingList[]{shopping_list}));
        Assert.assertEquals(1, shopping_book.getShopping_list().size());
    }

    @Test
    public void testShoppingList(){
        Item item1,item2,item3;

        ShoppingList shopping_list1=new ShoppingList();
        shopping_list1.setList_name("Groceries");
        item1=new Item( );
        item1.setItem_name("Pimm");
        item1.setDescription("Milk for break fast");
        item1.setPrice(1.30);

        item2=new Item();
        item2.setItem_name("Sai Leib");
        item2.setDescription("Withe bread");
        item2.setPrice(0.75);

        item3=new Item( );
        item2.setItem_name("Kama");
        item3.setDescription("Milk for break fast");
        item3.setPrice(0.55);

        shopping_list1.setItems_list(new ArrayList<Item>(Arrays.asList(new Item[]{item1, item2, item3})));
        Assert.assertNotNull(shopping_list1.getList_name());
        Assert.assertEquals(1.00,item1.getPrice(),item1.getPrice()-0.30);
        Assert.assertEquals(2.30,item1.getPrice(),item1.getPrice()-0.30);
        Assert.assertEquals(3,shopping_list1.getItems_list().size());

    }
    @Test
    public void shopinListAssembler(){
        ShoppingList shopping_list=new ShoppingList();
        shopping_list.setList_name("Test List");
        ShoppingListAssembler slist_assembler=new ShoppingListAssembler(shopping_list);

        String item1_id=slist_assembler.addItem("Potatoes");
        String item2_id=slist_assembler.addItem("Onions");
        String item3_id=slist_assembler.addItem("Garlic");
        String item4_id=slist_assembler.addItem("Bananas");
        Item it1=new Item();
        it1.setItem_name("Sibul");
        slist_assembler.updateItem(item2_id,it1);
        Assert.assertArrayEquals(new Object[]{item1_id, item2_id, item3_id, item4_id}, slist_assembler.getMdl_headers().toArray());
        Assert.assertEquals("Sibul", slist_assembler.getMdl_data_items().get(item2_id).get(0).getItem_name());
        slist_assembler.removeItem(item4_id);
        Assert.assertEquals(3,slist_assembler.getMdl_data_items().size());
        Assert.assertEquals(3,slist_assembler.getMdl_headers().size());

        List<Item> items=slist_assembler.getShopping_list().getItems_list();
        Item deleted_item = null;
        for(Item item:items){
          if(item4_id==item.getId()){deleted_item=item;break;}
        }
        Assert.assertNull(deleted_item);
        Assert.assertNull(slist_assembler.getMdl_data_items().get(item4_id));
    }

}
